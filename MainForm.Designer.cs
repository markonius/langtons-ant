﻿namespace LangtonsAnt
{
	partial class MainForm
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.components = new System.ComponentModel.Container();
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MainForm));
			this.stepButton = new System.Windows.Forms.Button();
			this.runButton = new System.Windows.Forms.Button();
			this.stopButton = new System.Windows.Forms.Button();
			this.newAntButton = new System.Windows.Forms.Button();
			this.ruleList = new System.Windows.Forms.TextBox();
			this.speedLabel = new System.Windows.Forms.Label();
			this.drawBoundsCheckBox = new System.Windows.Forms.CheckBox();
			this.rulesLabel = new System.Windows.Forms.Label();
			this.speedInput = new LangtonsAnt.NumericUpDownNoScroll();
			this.imageList1 = new System.Windows.Forms.ImageList(this.components);
			this.ruleErrorLabel = new System.Windows.Forms.Label();
			this.centerViewButton = new System.Windows.Forms.Button();
			this.editColorSchemeButton = new System.Windows.Forms.Button();
			((System.ComponentModel.ISupportInitialize)(this.speedInput)).BeginInit();
			this.SuspendLayout();
			// 
			// stepButton
			// 
			this.stepButton.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
			this.stepButton.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Silver;
			this.stepButton.FlatAppearance.MouseOverBackColor = System.Drawing.Color.White;
			this.stepButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
			this.stepButton.Location = new System.Drawing.Point(356, 11);
			this.stepButton.Name = "stepButton";
			this.stepButton.Size = new System.Drawing.Size(75, 23);
			this.stepButton.TabIndex = 0;
			this.stepButton.Text = "Step";
			this.stepButton.UseVisualStyleBackColor = false;
			this.stepButton.Click += new System.EventHandler(this.stepButton_Click);
			// 
			// runButton
			// 
			this.runButton.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
			this.runButton.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Silver;
			this.runButton.FlatAppearance.MouseOverBackColor = System.Drawing.Color.White;
			this.runButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
			this.runButton.Location = new System.Drawing.Point(194, 11);
			this.runButton.Name = "runButton";
			this.runButton.Size = new System.Drawing.Size(75, 23);
			this.runButton.TabIndex = 1;
			this.runButton.Text = "Run";
			this.runButton.UseVisualStyleBackColor = false;
			this.runButton.Click += new System.EventHandler(this.runButton_Click);
			// 
			// stopButton
			// 
			this.stopButton.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
			this.stopButton.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Silver;
			this.stopButton.FlatAppearance.MouseOverBackColor = System.Drawing.Color.White;
			this.stopButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
			this.stopButton.Location = new System.Drawing.Point(275, 11);
			this.stopButton.Name = "stopButton";
			this.stopButton.Size = new System.Drawing.Size(75, 23);
			this.stopButton.TabIndex = 2;
			this.stopButton.Text = "Stop";
			this.stopButton.UseVisualStyleBackColor = false;
			this.stopButton.Click += new System.EventHandler(this.stopButton_Click);
			// 
			// newAntButton
			// 
			this.newAntButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
			this.newAntButton.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
			this.newAntButton.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Silver;
			this.newAntButton.FlatAppearance.MouseOverBackColor = System.Drawing.Color.White;
			this.newAntButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
			this.newAntButton.Location = new System.Drawing.Point(679, 39);
			this.newAntButton.Name = "newAntButton";
			this.newAntButton.Size = new System.Drawing.Size(75, 23);
			this.newAntButton.TabIndex = 3;
			this.newAntButton.Text = "New Ant";
			this.newAntButton.UseVisualStyleBackColor = false;
			this.newAntButton.Click += new System.EventHandler(this.newAntButton_Click);
			// 
			// ruleList
			// 
			this.ruleList.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
			this.ruleList.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
			this.ruleList.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
			this.ruleList.Location = new System.Drawing.Point(113, 41);
			this.ruleList.Name = "ruleList";
			this.ruleList.Size = new System.Drawing.Size(560, 20);
			this.ruleList.TabIndex = 4;
			this.ruleList.Text = "LR";
			this.ruleList.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.ruleList_KeyPress);
			// 
			// speedLabel
			// 
			this.speedLabel.BackColor = System.Drawing.Color.Silver;
			this.speedLabel.Location = new System.Drawing.Point(12, 13);
			this.speedLabel.Name = "speedLabel";
			this.speedLabel.Size = new System.Drawing.Size(95, 20);
			this.speedLabel.TabIndex = 6;
			this.speedLabel.Text = "Steps per second:";
			this.speedLabel.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
			// 
			// drawBoundsCheckBox
			// 
			this.drawBoundsCheckBox.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
			this.drawBoundsCheckBox.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Silver;
			this.drawBoundsCheckBox.FlatAppearance.MouseOverBackColor = System.Drawing.Color.White;
			this.drawBoundsCheckBox.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
			this.drawBoundsCheckBox.Location = new System.Drawing.Point(437, 11);
			this.drawBoundsCheckBox.Name = "drawBoundsCheckBox";
			this.drawBoundsCheckBox.Padding = new System.Windows.Forms.Padding(3, 0, 0, 0);
			this.drawBoundsCheckBox.Size = new System.Drawing.Size(123, 23);
			this.drawBoundsCheckBox.TabIndex = 7;
			this.drawBoundsCheckBox.Text = "Draw board bounds";
			this.drawBoundsCheckBox.UseVisualStyleBackColor = false;
			this.drawBoundsCheckBox.CheckedChanged += new System.EventHandler(this.drawBoundsCheckBox_CheckedChanged);
			// 
			// rulesLabel
			// 
			this.rulesLabel.BackColor = System.Drawing.Color.Silver;
			this.rulesLabel.Location = new System.Drawing.Point(12, 41);
			this.rulesLabel.Name = "rulesLabel";
			this.rulesLabel.Size = new System.Drawing.Size(95, 20);
			this.rulesLabel.TabIndex = 8;
			this.rulesLabel.Text = "Ant rules:";
			this.rulesLabel.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
			// 
			// speedInput
			// 
			this.speedInput.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
			this.speedInput.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
			this.speedInput.Increment = new decimal(new int[] {
            100,
            0,
            0,
            0});
			this.speedInput.Location = new System.Drawing.Point(113, 13);
			this.speedInput.Name = "speedInput";
			this.speedInput.Size = new System.Drawing.Size(75, 20);
			this.speedInput.TabIndex = 9;
			this.speedInput.Value = new decimal(new int[] {
            100,
            0,
            0,
            0});
			this.speedInput.ValueChanged += new System.EventHandler(this.speedInput_ValueChanged);
			// 
			// imageList1
			// 
			this.imageList1.ColorDepth = System.Windows.Forms.ColorDepth.Depth8Bit;
			this.imageList1.ImageSize = new System.Drawing.Size(16, 16);
			this.imageList1.TransparentColor = System.Drawing.Color.Transparent;
			// 
			// ruleErrorLabel
			// 
			this.ruleErrorLabel.Anchor = System.Windows.Forms.AnchorStyles.Top;
			this.ruleErrorLabel.BackColor = System.Drawing.Color.Silver;
			this.ruleErrorLabel.ForeColor = System.Drawing.Color.Maroon;
			this.ruleErrorLabel.Location = new System.Drawing.Point(279, 68);
			this.ruleErrorLabel.Name = "ruleErrorLabel";
			this.ruleErrorLabel.Size = new System.Drawing.Size(180, 20);
			this.ruleErrorLabel.TabIndex = 10;
			this.ruleErrorLabel.Text = "Ant rules can only be \'L\' or \'R\'";
			this.ruleErrorLabel.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
			this.ruleErrorLabel.Visible = false;
			// 
			// centerViewButton
			// 
			this.centerViewButton.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
			this.centerViewButton.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Silver;
			this.centerViewButton.FlatAppearance.MouseOverBackColor = System.Drawing.Color.White;
			this.centerViewButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
			this.centerViewButton.Location = new System.Drawing.Point(566, 11);
			this.centerViewButton.Name = "centerViewButton";
			this.centerViewButton.Size = new System.Drawing.Size(75, 23);
			this.centerViewButton.TabIndex = 11;
			this.centerViewButton.Text = "Go to ant";
			this.centerViewButton.UseVisualStyleBackColor = false;
			this.centerViewButton.Click += new System.EventHandler(this.centerViewButton_Click);
			// 
			// editColorSchemeButton
			// 
			this.editColorSchemeButton.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
			this.editColorSchemeButton.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Silver;
			this.editColorSchemeButton.FlatAppearance.MouseOverBackColor = System.Drawing.Color.White;
			this.editColorSchemeButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
			this.editColorSchemeButton.Location = new System.Drawing.Point(647, 11);
			this.editColorSchemeButton.Name = "editColorSchemeButton";
			this.editColorSchemeButton.Size = new System.Drawing.Size(107, 23);
			this.editColorSchemeButton.TabIndex = 12;
			this.editColorSchemeButton.Text = "Edit color scheme";
			this.editColorSchemeButton.UseVisualStyleBackColor = false;
			this.editColorSchemeButton.Click += new System.EventHandler(this.editColorSchemeButton_Click);
			// 
			// MainForm
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(24)))), ((int)(((byte)(8)))));
			this.ClientSize = new System.Drawing.Size(766, 743);
			this.Controls.Add(this.editColorSchemeButton);
			this.Controls.Add(this.centerViewButton);
			this.Controls.Add(this.ruleErrorLabel);
			this.Controls.Add(this.speedInput);
			this.Controls.Add(this.rulesLabel);
			this.Controls.Add(this.drawBoundsCheckBox);
			this.Controls.Add(this.speedLabel);
			this.Controls.Add(this.ruleList);
			this.Controls.Add(this.newAntButton);
			this.Controls.Add(this.stopButton);
			this.Controls.Add(this.runButton);
			this.Controls.Add(this.stepButton);
			this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
			this.Location = new System.Drawing.Point(0, 0);
			this.MinimumSize = new System.Drawing.Size(782, 112);
			this.Name = "MainForm";
			this.Text = "Langton\'s Ant";
			this.ResizeBegin += new System.EventHandler(this.MainForm_ResizeBegin);
			this.ResizeEnd += new System.EventHandler(this.MainForm_ResizeEnd);
			this.MouseClick += new System.Windows.Forms.MouseEventHandler(this.MainForm_MouseClick);
			this.MouseDown += new System.Windows.Forms.MouseEventHandler(this.MainForm_MouseDown);
			this.MouseMove += new System.Windows.Forms.MouseEventHandler(this.MainForm_MouseMove);
			this.MouseUp += new System.Windows.Forms.MouseEventHandler(this.MainForm_MouseUp);
			this.Resize += new System.EventHandler(this.MainForm_Resize);
			((System.ComponentModel.ISupportInitialize)(this.speedInput)).EndInit();
			this.ResumeLayout(false);
			this.PerformLayout();

		}

		#endregion

		private System.Windows.Forms.Button stepButton;
		private System.Windows.Forms.Button runButton;
		private System.Windows.Forms.Button stopButton;
		private System.Windows.Forms.Button newAntButton;
		private System.Windows.Forms.TextBox ruleList;
		private System.Windows.Forms.Label speedLabel;
		private System.Windows.Forms.CheckBox drawBoundsCheckBox;
		private System.Windows.Forms.Label rulesLabel;
		private NumericUpDownNoScroll speedInput;
		private System.Windows.Forms.ImageList imageList1;
		private System.Windows.Forms.Label ruleErrorLabel;
		private System.Windows.Forms.Button centerViewButton;
		private System.Windows.Forms.Button editColorSchemeButton;
	}
}

