﻿using System.Drawing;
using System.Windows.Forms;

namespace LangtonsAnt
{
	public class ColorListView : ListView
	{
		protected override void OnDrawItem(DrawListViewItemEventArgs e)
		{
			base.OnDrawItem(e);

			ColorListViewItem colorItem = (ColorListViewItem)e.Item;

			e.Graphics.FillRectangle(new SolidBrush(colorItem.color), e.Bounds);

			Rectangle labelRectangle = new Rectangle(e.Bounds.X, e.Bounds.Y + e.Bounds.Height, e.Bounds.Width, 18);
			e.Graphics.DrawString(
				e.ItemIndex.ToString(), DefaultFont, Brushes.Black, labelRectangle,
				new StringFormat { Alignment = StringAlignment.Center, LineAlignment = StringAlignment.Center });

			if (e.Item.Selected)
			{
				float brightness = colorItem.color.GetBrightness();
				Color markerColor = brightness > 0.3f ? Color.Black : Color.White;
				e.Graphics.DrawRectangle(
					new Pen(markerColor, 4), new Rectangle(
						e.Bounds.X + 2,
						e.Bounds.Y + 2,
						e.Bounds.Width - 4,
						e.Bounds.Height - 4));
			}
		}
	}
}
