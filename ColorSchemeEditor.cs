﻿using System;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Windows.Forms;
using System.Xml.Serialization;

namespace LangtonsAnt
{
	public partial class ColorSchemeEditor : Form
	{
		public const string DEFAULT_COLOR_SCHEME = "default.csc";

		private readonly ColorScheme colorScheme;
		private readonly Random random;

		public ColorSchemeEditor(ColorScheme colorScheme, Random random)
		{
			InitializeComponent();

			this.colorScheme = colorScheme;
			this.random = random;

			PopulateColorView(colorScheme);
		}

		private void PopulateColorView(ColorScheme colorScheme)
		{
			foreach (Color color in colorScheme.colors)
				colorListView.Items.Add(new ColorListViewItem(color));
		}

		private void ChangeColor()
		{
			if (colorListView.SelectedItems.Count > 0)
			{
				Color oldColor = ((ColorListViewItem)colorListView.SelectedItems[0]).color;
				var colorPicker = new ColorDialog
				{
					Color = oldColor,
					CustomColors = colorListView.Items.Cast<ColorListViewItem>()
						.Select(c => c.color.B << 16 | c.color.G << 8 | c.color.R)
						.Take(16).ToArray()
				};
				DialogResult result = colorPicker.ShowDialog(this);
				if (result == DialogResult.OK)
				{
					foreach (ColorListViewItem item in colorListView.SelectedItems)
						item.color = colorPicker.Color;
				}
			}
			Refresh();
		}

		public static int[] LoadColorScheme(string filePath)
		{
			var serializer = new XmlSerializer(typeof(int[]));
			using (var reader = new StreamReader(filePath))
				return (int[])serializer.Deserialize(reader);
		}

		public T GetFileDialog<T>() where T : FileDialog, new()
		{
			return new T
			{
				DefaultExt = "csc",
				FileName = DEFAULT_COLOR_SCHEME,
				InitialDirectory = Environment.CurrentDirectory,
				Filter = "Color scheme|*.csc"
			};
		}

		private void Apply()
		{
			colorScheme.colors = colorListView.Items.Cast<ColorListViewItem>()
				.Select(c => c.color).ToArray();
		}

		private void colorListView_ItemSelectionChanged(object sender, ListViewItemSelectionChangedEventArgs e)
		{
			removeButton.Enabled = moveLeftButton.Enabled = moveRightButton.Enabled = changeColorButton.Enabled
				= colorListView.SelectedItems.Count > 0;
		}

		private void addButton_Click(object sender, EventArgs e)
		{
			var newItem = new ColorListViewItem(Color.FromArgb(random.Next(256), random.Next(256), random.Next(256)));
			if (colorListView.SelectedItems.Count > 0)
			{
				int index = colorListView.SelectedIndices[0];
				colorListView.Items.Insert(index, newItem);
			}
			else
			{
				colorListView.Items.Add(newItem);
			}
		}

		private void removeButton_Click(object sender, EventArgs e)
		{
			while (colorListView.SelectedIndices.Count > 0)
				colorListView.Items.RemoveAt(colorListView.SelectedIndices[0]);
		}

		private void moveLeftButton_Click(object sender, EventArgs e)
		{
			int minIndex = 0;
			foreach (int index in colorListView.SelectedIndices.Cast<int>().ToArray())
			{
				if (index == minIndex)
				{
					minIndex++;
					continue;
				}

				ListViewItem item = colorListView.Items[index];
				colorListView.Items.RemoveAt(index);
				colorListView.Items.Insert(index - 1, item);
			}
		}

		private void moveRightButton_Click(object sender, EventArgs e)
		{
			int maxIndex = colorListView.Items.Count - 1;
			foreach (int index in colorListView.SelectedIndices.Cast<int>().ToArray().Reverse())
			{
				if (index == maxIndex)
				{
					maxIndex--;
					continue;
				}

				ListViewItem item = colorListView.Items[index];
				colorListView.Items.RemoveAt(index);
				colorListView.Items.Insert(index + 1, item);
			}
		}

		private void colorListView_DoubleClick(object sender, EventArgs e)
		{
			ChangeColor();
		}

		private void changeColorButton_Click(object sender, EventArgs e)
		{
			ChangeColor();
		}

		private void saveSchemeButton_Click(object sender, EventArgs e)
		{
			var saveFileDialog = GetFileDialog<SaveFileDialog>();

			DialogResult result = saveFileDialog.ShowDialog(this);
			if (result == DialogResult.OK)
			{
				var serializer = new XmlSerializer(typeof(int[]));
				using (StreamWriter writer = new StreamWriter(saveFileDialog.FileName))
					serializer.Serialize(writer, colorScheme.EncodedColors);
			}
		}

		private void loadSchemeButton_Click(object sender, EventArgs e)
		{
			var loadFileDialog = GetFileDialog<OpenFileDialog>();

			DialogResult result = loadFileDialog.ShowDialog(this);
			if (result == DialogResult.OK)
			{
				colorScheme.EncodedColors = LoadColorScheme(loadFileDialog.FileName);
				colorListView.Items.Clear();
				PopulateColorView(colorScheme);
			}
		}

		private void applyButton_Click(object sender, EventArgs e)
		{
			Apply();
		}
	}
}
