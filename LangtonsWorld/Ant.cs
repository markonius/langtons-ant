﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace LangtonsAnt.LangtonsWorld
{
	public class Ant : IDisposable
	{
		public const double MAX_SPEED = 10_000_000;

		private readonly Rotation[] rules;
		public readonly Board board;

		private Rotation rotation = Rotation._forward;
		public double stepsPerSecond = 10;

		private Vector Forward => rotation * Vector._up;
		public Vector Position { get; private set; }
		public bool Running { get; private set; }
		public IReadOnlyList<Rotation> Rules => rules;

		public Ant(IEnumerable<Rotation> rules)
		{
			this.rules = rules.ToArray();
			board = new Board();
		}

		public void Step()
		{
			lock (this)
			{
				Cell cell = board[Position];
				Rotation rule = rules[cell];
				board[Position] = (uint)((cell + 1) % rules.Length);
				rotation *= rule;
				Position += Forward;
			}
		}

		public void Run()
		{
			Running = true;
			Task.Run(
				() =>
				{
					while (Running)
					{
						if (stepsPerSecond >= MAX_SPEED)
						{
							Step();
						}
						else
						{
							int millisecondsPerStep = Math.Max((int)(1000 / stepsPerSecond), 1);
							Thread.Sleep(millisecondsPerStep);

							int stepsPerMillisecond = Math.Max((int)stepsPerSecond / 1000, 1);
							for (int i = 0; i < stepsPerMillisecond; i++)
								Step();
						}
					}
				}
			);
		}

		public void Stop()
		{
			Running = false;
		}

		private void ReleaseUnmanagedResources()
		{
			Running = false;
		}

		public void Dispose()
		{
			ReleaseUnmanagedResources();
			GC.SuppressFinalize(this);
		}

		~Ant()
		{
			ReleaseUnmanagedResources();
		}
	}
}
