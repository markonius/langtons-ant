﻿using System;

namespace LangtonsAnt.LangtonsWorld
{
	public struct Cell
	{
		private readonly uint value;

		private Cell(uint value)
		{
			this.value = value;
		}

		public bool Equals(Cell other)
		{
			return value == other.value;
		}

		public override bool Equals(object obj)
		{
			if (ReferenceEquals(null, obj)) return false;
			return obj is Cell other && Equals(other);
		}

		public override int GetHashCode()
		{
			return (int)value;
		}

		public static bool operator ==(Cell left, Cell right)
		{
			return left.Equals(right);
		}

		public static bool operator !=(Cell left, Cell right)
		{
			return !left.Equals(right);
		}

		public static implicit operator uint(Cell cell)
		{
			return cell.value;
		}

		public static implicit operator Cell(uint value)
		{
			return new Cell(value);
		}

		public static implicit operator Cell(int value)
		{
			if (value >= 0)
				return new Cell((uint)value);
			else
				throw new ArgumentOutOfRangeException(nameof(value), "Value cannot be less than zero.");
		}
	}
}
