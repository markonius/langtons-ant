﻿/*
* MIT License
*
* Copyright (c) 2009-2018 Jingwood, unvell.com. All right reserved.
*
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files (the "Software"), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions:
*
* The above copyright notice and this permission notice shall be included in all
* copies or substantial portions of the Software.
*
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
* AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
* OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
* SOFTWARE.
*/

using System;
using System.Windows.Forms;
using unvell.D2DLib;
using unvell.D2DLib.WinForm;

namespace LangtonsAnt.LangtonsWorld
{
	public class D2DForm : Form
	{
		private D2DDevice device;
		public D2DDevice Device
		{
			get
			{
				IntPtr hwnd = Handle;
				return device ?? (device = D2DDevice.FromHwnd(hwnd));
			}
		}

		private D2DBitmap backgroundImage;
		new public D2DBitmap BackgroundImage
		{
			get => backgroundImage;
			set
			{
				if (backgroundImage != value)
				{
					backgroundImage?.Dispose();
					backgroundImage = value;
					Invalidate();
				}
			}
		}

		private D2DGraphics graphics;
		protected bool autoClearBuffer = true;
		protected bool Antialias
		{
			get => graphics.Antialias;
			set => graphics.Antialias = value;
		}

		private int currentFps;
		private int lastFps;
		public bool ShowFPS { get; set; }
		private DateTime lastFpsUpdate = DateTime.Now;

		private readonly Timer timer = new Timer { Interval = 10 };
		public bool EscapeKeyToClose { get; set; } = true;

		private bool animationDraw;
		public bool AnimationDraw
		{
			get => animationDraw;
			set
			{
				animationDraw = value;
				if (!animationDraw && !sceneAnimation && timer.Enabled) timer.Stop();
			}
		}

		private bool sceneAnimation;

		protected bool SceneAnimation
		{
			get => sceneAnimation;
			set
			{
				sceneAnimation = value;
				if (!animationDraw && !sceneAnimation && timer.Enabled) timer.Stop();
			}
		}
		protected bool SceneChanged { get; set; }

		protected override void CreateHandle()
		{
			base.CreateHandle();

			DoubleBuffered = false;

			if (device == null)
			{
				device = D2DDevice.FromHwnd(Handle);
			}

			graphics = new D2DGraphics(device);
			timer.Tick += (ss, ee) =>
			{
				if (!SceneAnimation || SceneChanged)
				{
					OnFrame();
					Invalidate();
					SceneChanged = false;
				}
			};
		}

		protected override void OnPaintBackground(PaintEventArgs e) { }

		protected override void OnPaint(PaintEventArgs e)
		{
			if (DesignMode)
			{
				e.Graphics.Clear(System.Drawing.Color.Black);
				e.Graphics.DrawString("D2DLib windows form cannot render in design time.", Font, System.Drawing.Brushes.White, 10, 10);
			}
			else
			{
				if (backgroundImage != null)
				{
					graphics.BeginRender(backgroundImage);
				}
				else
				{
					if (autoClearBuffer)
						graphics.BeginRender(D2DColor.FromGDIColor(BackColor));
					else
						graphics.BeginRender();
				}

				OnRender(graphics);

				if (ShowFPS)
				{
					if (lastFpsUpdate.Second != DateTime.Now.Second)
					{
						lastFps = currentFps;
						currentFps = 0;
						lastFpsUpdate = DateTime.Now;
					}
					else
					{
						currentFps++;
					}

					string fpsInfo = $"{lastFps} fps";
					System.Drawing.SizeF size = e.Graphics.MeasureString(fpsInfo, Font, Width);
					graphics.DrawText(
						fpsInfo, D2DColor.Silver, Font,
						new System.Drawing.PointF(ClientRectangle.Right - size.Width - 10, 5));
				}

				graphics.EndRender();

				if ((animationDraw || sceneAnimation) && !timer.Enabled)
				{
					timer.Start();
				}
			}
		}

		protected override void WndProc(ref Message m)
		{
			switch (m.Msg)
			{
				case (int)unvell.Common.Win32Lib.Win32.WMessages.WM_ERASEBKGND:
					break;

				case (int)unvell.Common.Win32Lib.Win32.WMessages.WM_SIZE:
					base.WndProc(ref m);
					if (device != null)
					{
						device.Resize();
						Invalidate(false);
					}
					break;

				case (int)unvell.Common.Win32Lib.Win32.WMessages.WM_DESTROY:
					backgroundImage?.Dispose();
					device?.Dispose();
					base.WndProc(ref m);
					break;

				default:
					base.WndProc(ref m);
					break;
			}
		}

		protected virtual void OnRender(D2DGraphics g) { }

		protected virtual void OnFrame() { }

		new public void Invalidate()
		{
			Invalidate(false);
		}

		protected override void OnKeyDown(KeyEventArgs e)
		{
			base.OnKeyDown(e);

			switch (e.KeyCode)
			{
				case Keys.Escape:
					if (EscapeKeyToClose) Close();
					break;
			}
		}
	}

	public class D2DControl : Control
	{
		private D2DDevice device;

		public D2DDevice Device
		{
			get
			{
				IntPtr hwnd = Handle;
				return device ?? (device = D2DDevice.FromHwnd(hwnd));
			}
		}

		private D2DGraphics graphics;

		private int currentFps;
		private int lastFps;
		public bool ShowFPS { get; set; }
		private DateTime lastFpsUpdate = DateTime.Now;

		protected override void CreateHandle()
		{
			base.CreateHandle();

			DoubleBuffered = false;

			if (device == null)
			{
				device = D2DDevice.FromHwnd(Handle);
			}

			graphics = new D2DGraphics(device);
		}

		private D2DBitmap backgroundImage = null;

		new public D2DBitmap BackgroundImage
		{
			get => backgroundImage;
			set
			{
				if (backgroundImage != value)
				{
					backgroundImage?.Dispose();
					backgroundImage = value;
					Invalidate();
				}
			}
		}

		protected override void OnPaintBackground(PaintEventArgs e) { }

		protected override void OnPaint(PaintEventArgs e)
		{
			if (backgroundImage != null)
			{
				graphics.BeginRender(backgroundImage);
			}
			else
			{
				graphics.BeginRender(D2DColor.FromGDIColor(BackColor));
			}

			OnRender(graphics);

			if (ShowFPS)
			{
				if (lastFpsUpdate.Second != DateTime.Now.Second)
				{
					lastFps = currentFps;
					currentFps = 0;
				}
				else
				{
					currentFps++;
				}

				string info = $"{lastFps} fps";
				System.Drawing.SizeF size = e.Graphics.MeasureString(info, Font, Width);
				e.Graphics.DrawString(info, Font, System.Drawing.Brushes.Black, ClientRectangle.Right - size.Width - 10, 5);
			}

			graphics.EndRender();
		}

		protected override void DestroyHandle()
		{
			base.DestroyHandle();
			device.Dispose();
		}

		protected virtual void OnRender(D2DGraphics g) { }

		protected override void WndProc(ref Message m)
		{
			switch (m.Msg)
			{
				case (int)unvell.Common.Win32Lib.Win32.WMessages.WM_ERASEBKGND:
					break;

				case (int)unvell.Common.Win32Lib.Win32.WMessages.WM_SIZE:
					base.WndProc(ref m);
					device?.Resize();
					break;

				default:
					base.WndProc(ref m);
					break;
			}
		}

		new public void Invalidate()
		{
			Invalidate(false);
		}
	}
}
