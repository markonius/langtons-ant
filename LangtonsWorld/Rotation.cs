﻿namespace LangtonsAnt.LangtonsWorld
{
	public struct Rotation
	{
		public static readonly Rotation _forward = new Rotation(new sbyte[,] { { 1, 0 }, { 0, 1 } });
		public static readonly Rotation _right = new Rotation(new sbyte[,] { { 0, 1 }, { -1, 0 } });
		public static readonly Rotation _left = new Rotation(new sbyte[,] { { 0, -1 }, { 1, 0 } });
		public static readonly Rotation _back = new Rotation(new sbyte[,] { { -1, 0 }, { 0, -1 } });

		private readonly sbyte[,] matrix;

		private Rotation(sbyte[,] matrix)
		{
			this.matrix = matrix;
		}

		public bool Equals(Rotation other)
		{
			return matrix[0, 0] == other.matrix[0, 0]
				&& matrix[0, 1] == other.matrix[0, 1]
				&& matrix[1, 1] == other.matrix[1, 1]
				&& matrix[1, 0] == other.matrix[1, 0]
				;
		}

		public override bool Equals(object obj)
		{
			if (ReferenceEquals(null, obj)) return false;
			return obj is Rotation other && Equals(other);
		}

		public override int GetHashCode()
		{
			unchecked
			{
				return (((((matrix[0, 0] * 397) ^ matrix[0, 1]) * 397) ^ matrix[1, 1]) * 397) ^ matrix[1, 0];
			}
		}

		public static bool operator ==(Rotation left, Rotation right)
		{
			return left.Equals(right);
		}

		public static bool operator !=(Rotation left, Rotation right)
		{
			return !left.Equals(right);
		}

		public static Vector operator *(Rotation rotation, Vector vector)
		{
			return new Vector(
				vector.x * rotation.matrix[0, 0] + vector.y * rotation.matrix[1, 0],
				vector.x * rotation.matrix[0, 1] + vector.y * rotation.matrix[1, 1]
			);
		}

		public static Rotation operator *(Rotation second, Rotation first)
		{
			var a = second.matrix;
			var b = first.matrix;

			return new Rotation(
				new sbyte[,]
				{
					{ (sbyte)(a[0, 0] * b[0, 0] + a[0, 1] * b[1, 0]), (sbyte)(a[0, 0] * b[0, 1] + a[0, 1] * b[1, 1]) },
					{ (sbyte)(a[0, 0] * b[0, 1] + a[0, 1] * b[1, 1]), (sbyte)(a[1, 0] * b[0, 1] + a[1, 1] * b[1, 1]) },
				}
			);
		}
	}
}
