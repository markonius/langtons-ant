﻿namespace LangtonsAnt.LangtonsWorld
{
	public class Board
	{
		private const int DEFAULT_SIZE = 16;
		private const int DEFAULT_OFFSET = DEFAULT_SIZE / 2;

		private Cell[,] cells = new Cell[DEFAULT_SIZE, DEFAULT_SIZE];
		private Vector offset = new Vector(DEFAULT_OFFSET, DEFAULT_OFFSET);

		private int SizeX => cells.GetLength(0);
		private int SizeY => cells.GetLength(1);
		public Vector Size => new Vector(SizeX, SizeY);
		public Bounds Bounds => new Bounds(-offset.x, Size.x - offset.x, -offset.y, Size.y - offset.y);

		private void Embiggen(int x, int y)
		{
			var newSize = new Vector(
				x < 0 || x >= SizeX ? SizeX * 2 : SizeX,
				y < 0 || y >= SizeY ? SizeY * 2 : SizeY
			);
			var newCells = new Cell[newSize.x, newSize.y];
			var newOffset = new Vector(
				x > 0 ? offset.x : newSize.x - (SizeX - offset.x),
				y > 0 ? offset.y : newSize.y - (SizeY - offset.y)
			);

			for (int oldY = 0; oldY < SizeY; oldY++)
			{
				for (int oldX = 0; oldX < SizeX; oldX++)
				{
					int newX = oldX - offset.x + newOffset.x;
					int newY = oldY - offset.y + newOffset.y;
					newCells[newX, newY] = cells[oldX, oldY];
				}
			}

			cells = newCells;
			offset = newOffset;
		}

		public Cell this[Vector position]
		{
			get => this[position.x, position.y];
			set => this[position.x, position.y] = value;
		}

		public Cell this[int x, int y]
		{
			get
			{
				int localX = x + offset.x;
				int localY = y + offset.y;

				if (localX >= 0 && localX < SizeX && localY >= 0 && localY < SizeY)
					return cells[localX, localY];
				else
					return default(Cell);
			}

			set
			{
				int localX = x + offset.x;
				int localY = y + offset.y;

				if (localX >= 0 && localX < SizeX && localY >= 0 && localY < SizeY)
				{
					cells[localX, localY] = value;
				}
				else
				{
					Embiggen(localX, localY);
					this[x, y] = value;
				}
			}
		}
	}
}
