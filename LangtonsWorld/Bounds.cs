﻿using System.Collections;
using System.Collections.Generic;

namespace LangtonsAnt.LangtonsWorld
{
	public struct Bounds : IEnumerable<Vector>
	{
		public readonly int xMin;
		public readonly int xMax;
		public readonly int yMin;
		public readonly int yMax;

		public Bounds(int xMin, int xMax, int yMin, int yMax)
		{
			this.xMin = xMin;
			this.xMax = xMax;
			this.yMin = yMin;
			this.yMax = yMax;
		}

		public IEnumerator<Vector> GetEnumerator()
		{
			for (int y = yMin; y <= yMax; y++)
			{
				for (int x = xMin; x <= xMax; x++)
					yield return (x, y);
			}
		}

		IEnumerator IEnumerable.GetEnumerator()
		{
			return GetEnumerator();
		}
	}
}
