﻿namespace LangtonsAnt.LangtonsWorld
{
	public struct Vector
	{
		public static readonly Vector _up = new Vector(0, 1);
		public static readonly Vector _right = new Vector(1, 0);
		public static readonly Vector _left = new Vector(-1, 0);
		public static readonly Vector _down = new Vector(0, -1);

		public readonly int x;
		public readonly int y;

		public Vector(int x, int y)
		{
			this.x = x;
			this.y = y;
		}

		public bool Equals(Vector other)
		{
			return x == other.x && y == other.y;
		}

		public override bool Equals(object obj)
		{
			if (ReferenceEquals(null, obj)) return false;
			return obj is Vector other && Equals(other);
		}

		public override int GetHashCode()
		{
			unchecked
			{
				return (x * 397) ^ y;
			}
		}

		public static bool operator ==(Vector left, Vector right)
		{
			return left.Equals(right);
		}

		public static bool operator !=(Vector left, Vector right)
		{
			return !left.Equals(right);
		}

		public static implicit operator Vector((int, int) tuple)
		{
			return new Vector(tuple.Item1, tuple.Item2);
		}

		public static implicit operator (int x, int y)(Vector position)
		{
			return (position.x, position.y);
		}

		public override string ToString()
		{
			return $"({x:#.##}, {y:#.##})";
		}

		public static Vector operator +(Vector left, Vector right)
		{
			return new Vector(left.x + right.x, left.y + right.y);
		}

		public static Vector operator *(int factor, Vector vector)
		{
			return new Vector(vector.x * factor, vector.y * factor);
		}
	}
}
