﻿using System;
using System.Threading;
using System.Windows.Forms;

namespace LangtonsAnt
{
	public static class SimplifiedInvoker
	{
		public static void Invoke(this Control control, Action action)
		{
			{
				int safety = 0;
				while (!control.IsHandleCreated && safety < 1000)
				{
					safety++;
					Thread.Sleep(10);
				}
				if (safety >= 1000)
					throw new Exception("Handle of window is not created.");
			}
			control.Invoke((MethodInvoker)delegate { action(); });
		}
	}
}
