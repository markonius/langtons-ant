﻿using LangtonsAnt.LangtonsWorld;

namespace LangtonsAnt
{
	public struct VectorD
	{
		public readonly double x;
		public readonly double y;

		public VectorD(double x, double y)
		{
			this.x = x;
			this.y = y;
		}

		public override string ToString()
		{
			return $"({x:#.##}, {y:#.##})";
		}

		public static VectorD operator +(VectorD left, VectorD right)
		{
			return new VectorD(left.x + right.x, left.y + right.y);
		}

		public static VectorD operator -(VectorD left, VectorD right)
		{
			return new VectorD(left.x - right.x, left.y - right.y);
		}

		public static VectorD operator *(double factor, VectorD vector)
		{
			return new VectorD(vector.x * factor, vector.y * factor);
		}

		public static VectorD operator /(VectorD vector, double factor)
		{
			return 1 / factor * vector;
		}

	public static implicit operator VectorD(Vector vector)
		{
			return new VectorD(vector.x, vector.y);
		}

		public bool Equals(VectorD other)
		{
			return x.Equals(other.x) && y.Equals(other.y);
		}

		public override bool Equals(object obj)
		{
			if (ReferenceEquals(null, obj)) return false;
			return obj is VectorD other && Equals(other);
		}

		public override int GetHashCode()
		{
			unchecked
			{
				return (x.GetHashCode() * 397) ^ y.GetHashCode();
			}
		}

		public static bool operator ==(VectorD left, VectorD right)
		{
			return left.Equals(right);
		}

		public static bool operator !=(VectorD left, VectorD right)
		{
			return !left.Equals(right);
		}
	}
}
