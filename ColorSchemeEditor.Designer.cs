﻿namespace LangtonsAnt
{
	partial class ColorSchemeEditor
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ColorSchemeEditor));
			this.saveSchemeButton = new System.Windows.Forms.Button();
			this.addButton = new System.Windows.Forms.Button();
			this.removeButton = new System.Windows.Forms.Button();
			this.moveLeftButton = new System.Windows.Forms.Button();
			this.moveRightButton = new System.Windows.Forms.Button();
			this.changeColorButton = new System.Windows.Forms.Button();
			this.loadSchemeButton = new System.Windows.Forms.Button();
			this.colorListView = new LangtonsAnt.ColorListView();
			this.applyButton = new System.Windows.Forms.Button();
			this.SuspendLayout();
			// 
			// saveSchemeButton
			// 
			this.saveSchemeButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
			this.saveSchemeButton.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
			this.saveSchemeButton.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Silver;
			this.saveSchemeButton.FlatAppearance.MouseOverBackColor = System.Drawing.Color.White;
			this.saveSchemeButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
			this.saveSchemeButton.Location = new System.Drawing.Point(257, 250);
			this.saveSchemeButton.Name = "saveSchemeButton";
			this.saveSchemeButton.Size = new System.Drawing.Size(60, 23);
			this.saveSchemeButton.TabIndex = 7;
			this.saveSchemeButton.Text = " Save";
			this.saveSchemeButton.UseVisualStyleBackColor = false;
			this.saveSchemeButton.Click += new System.EventHandler(this.saveSchemeButton_Click);
			// 
			// addButton
			// 
			this.addButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
			this.addButton.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
			this.addButton.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Silver;
			this.addButton.FlatAppearance.MouseOverBackColor = System.Drawing.Color.White;
			this.addButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
			this.addButton.Location = new System.Drawing.Point(12, 250);
			this.addButton.Name = "addButton";
			this.addButton.Size = new System.Drawing.Size(22, 23);
			this.addButton.TabIndex = 1;
			this.addButton.Text = "+";
			this.addButton.UseVisualStyleBackColor = false;
			this.addButton.Click += new System.EventHandler(this.addButton_Click);
			// 
			// removeButton
			// 
			this.removeButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
			this.removeButton.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
			this.removeButton.Enabled = false;
			this.removeButton.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Silver;
			this.removeButton.FlatAppearance.MouseOverBackColor = System.Drawing.Color.White;
			this.removeButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
			this.removeButton.Location = new System.Drawing.Point(40, 250);
			this.removeButton.Name = "removeButton";
			this.removeButton.Size = new System.Drawing.Size(22, 23);
			this.removeButton.TabIndex = 2;
			this.removeButton.Text = "-";
			this.removeButton.UseVisualStyleBackColor = false;
			this.removeButton.Click += new System.EventHandler(this.removeButton_Click);
			// 
			// moveLeftButton
			// 
			this.moveLeftButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
			this.moveLeftButton.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
			this.moveLeftButton.Enabled = false;
			this.moveLeftButton.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Silver;
			this.moveLeftButton.FlatAppearance.MouseOverBackColor = System.Drawing.Color.White;
			this.moveLeftButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
			this.moveLeftButton.Location = new System.Drawing.Point(68, 250);
			this.moveLeftButton.Name = "moveLeftButton";
			this.moveLeftButton.Size = new System.Drawing.Size(22, 23);
			this.moveLeftButton.TabIndex = 3;
			this.moveLeftButton.Text = "<";
			this.moveLeftButton.UseVisualStyleBackColor = false;
			this.moveLeftButton.Click += new System.EventHandler(this.moveLeftButton_Click);
			// 
			// moveRightButton
			// 
			this.moveRightButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
			this.moveRightButton.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
			this.moveRightButton.Enabled = false;
			this.moveRightButton.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Silver;
			this.moveRightButton.FlatAppearance.MouseOverBackColor = System.Drawing.Color.White;
			this.moveRightButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
			this.moveRightButton.Location = new System.Drawing.Point(96, 250);
			this.moveRightButton.Name = "moveRightButton";
			this.moveRightButton.Size = new System.Drawing.Size(22, 23);
			this.moveRightButton.TabIndex = 4;
			this.moveRightButton.Text = ">";
			this.moveRightButton.UseVisualStyleBackColor = false;
			this.moveRightButton.Click += new System.EventHandler(this.moveRightButton_Click);
			// 
			// changeColorButton
			// 
			this.changeColorButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
			this.changeColorButton.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
			this.changeColorButton.Enabled = false;
			this.changeColorButton.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Silver;
			this.changeColorButton.FlatAppearance.MouseOverBackColor = System.Drawing.Color.White;
			this.changeColorButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
			this.changeColorButton.Location = new System.Drawing.Point(124, 250);
			this.changeColorButton.Name = "changeColorButton";
			this.changeColorButton.Size = new System.Drawing.Size(60, 23);
			this.changeColorButton.TabIndex = 5;
			this.changeColorButton.Text = "Set color";
			this.changeColorButton.UseVisualStyleBackColor = false;
			this.changeColorButton.Click += new System.EventHandler(this.changeColorButton_Click);
			// 
			// loadSchemeButton
			// 
			this.loadSchemeButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
			this.loadSchemeButton.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
			this.loadSchemeButton.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Silver;
			this.loadSchemeButton.FlatAppearance.MouseOverBackColor = System.Drawing.Color.White;
			this.loadSchemeButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
			this.loadSchemeButton.Location = new System.Drawing.Point(191, 250);
			this.loadSchemeButton.Name = "loadSchemeButton";
			this.loadSchemeButton.Size = new System.Drawing.Size(60, 23);
			this.loadSchemeButton.TabIndex = 6;
			this.loadSchemeButton.Text = "Load";
			this.loadSchemeButton.UseVisualStyleBackColor = false;
			this.loadSchemeButton.Click += new System.EventHandler(this.loadSchemeButton_Click);
			// 
			// colorListView
			// 
			this.colorListView.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
			this.colorListView.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
			this.colorListView.ForeColor = System.Drawing.SystemColors.WindowText;
			this.colorListView.Location = new System.Drawing.Point(0, 0);
			this.colorListView.Name = "colorListView";
			this.colorListView.OwnerDraw = true;
			this.colorListView.Size = new System.Drawing.Size(395, 240);
			this.colorListView.Sorting = System.Windows.Forms.SortOrder.Ascending;
			this.colorListView.TabIndex = 0;
			this.colorListView.UseCompatibleStateImageBehavior = false;
			this.colorListView.ItemSelectionChanged += new System.Windows.Forms.ListViewItemSelectionChangedEventHandler(this.colorListView_ItemSelectionChanged);
			this.colorListView.DoubleClick += new System.EventHandler(this.colorListView_DoubleClick);
			// 
			// applyButton
			// 
			this.applyButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
			this.applyButton.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
			this.applyButton.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Silver;
			this.applyButton.FlatAppearance.MouseOverBackColor = System.Drawing.Color.White;
			this.applyButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
			this.applyButton.Location = new System.Drawing.Point(323, 250);
			this.applyButton.Name = "applyButton";
			this.applyButton.Size = new System.Drawing.Size(60, 23);
			this.applyButton.TabIndex = 8;
			this.applyButton.Text = "Apply";
			this.applyButton.UseVisualStyleBackColor = false;
			this.applyButton.Click += new System.EventHandler(this.applyButton_Click);
			// 
			// ColorSchemeEditor
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.BackColor = System.Drawing.Color.Silver;
			this.ClientSize = new System.Drawing.Size(395, 281);
			this.Controls.Add(this.applyButton);
			this.Controls.Add(this.loadSchemeButton);
			this.Controls.Add(this.changeColorButton);
			this.Controls.Add(this.moveRightButton);
			this.Controls.Add(this.moveLeftButton);
			this.Controls.Add(this.removeButton);
			this.Controls.Add(this.addButton);
			this.Controls.Add(this.colorListView);
			this.Controls.Add(this.saveSchemeButton);
			this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.SizableToolWindow;
			this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
			this.MinimumSize = new System.Drawing.Size(411, 160);
			this.Name = "ColorSchemeEditor";
			this.Text = "Color Scheme Editor";
			this.ResumeLayout(false);

		}

		#endregion

		private ColorListView colorListView;
		private System.Windows.Forms.Button applyButton;
		private System.Windows.Forms.Button saveSchemeButton;
		private System.Windows.Forms.Button loadSchemeButton;
		private System.Windows.Forms.Button addButton;
		private System.Windows.Forms.Button removeButton;
		private System.Windows.Forms.Button moveLeftButton;
		private System.Windows.Forms.Button moveRightButton;
		private System.Windows.Forms.Button changeColorButton;
	}
}