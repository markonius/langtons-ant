﻿using LangtonsAnt.LangtonsWorld;

namespace LangtonsAnt
{
	public struct RectD
	{
		public readonly double x;
		public readonly double y;
		public readonly double width;
		public readonly double height;

		public RectD(double x, double y, double width, double height)
		{
			this.x = x;
			this.y = y;
			this.width = width;
			this.height = height;
		}

		public RectD(VectorD position, VectorD size)
		{
			x = position.x;
			y = position.y;
			width = size.x;
			height = size.y;
		}

		public static explicit operator Bounds(RectD rect)
		{
			return new Bounds((int)rect.x, (int)(rect.x + rect.width), (int)rect.y, (int)(rect.y + rect.height));
		}
	}
}
