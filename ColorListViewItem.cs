﻿using System.Drawing;
using System.Windows.Forms;

namespace LangtonsAnt
{
	public class ColorListViewItem : ListViewItem
	{
		public Color color;

		public ColorListViewItem(Color color)
		{
			this.color = color;
		}
	}
}
