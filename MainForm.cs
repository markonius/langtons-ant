﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;
using LangtonsAnt.LangtonsWorld;
using unvell.D2DLib;

namespace LangtonsAnt
{
	public partial class MainForm : D2DForm
	{
		private const int IMPORTANT_FRAME_TIME = 8;
		private const int COMBINED_FRAME_TIME = 16;

		private const double ZOOM_SPEED = 0.001;

		private readonly Random random = new Random(0);

		private ColorSchemeEditor colorSchemeEditor;
		private readonly ColorScheme colorScheme = new ColorScheme();

		private Ant ant;
		private readonly D2DColor antFillColor = D2DColor.GhostWhite;
		private readonly D2DColor antBorderColor = D2DColor.DimGray;
		private D2DColor[] colors;
		private bool drawBounds;

		private double zoomLevel = 16;
		private VectorD viewPosition;
		private RectD view;

		private bool panning;
		private VectorD previousMousePosition;

		private int unimportantRenderPosition;
		private int maxUnimportantRenderBlockSize;
		private bool movingWindow;

		private int passesToRenderLeft;
		private bool DoneRendering => passesToRenderLeft == 0;

		private Action<D2DGraphics> toRender;

		public MainForm()
		{
			InitializeComponent();
			autoClearBuffer = false;

			MouseWheel += Zoom;
			speedInput.Maximum = (decimal)Ant.MAX_SPEED;
			speedInput.MouseWheel += LooseSpeedInputFocus;
			foreach (Control control in Controls)
			{
				control.MouseMove += RefreshControl;
				control.MouseLeave += RefreshControl;
			}

			try
			{
				if (File.Exists(ColorSchemeEditor.DEFAULT_COLOR_SCHEME))
					colorScheme.EncodedColors = ColorSchemeEditor.LoadColorScheme(ColorSchemeEditor.DEFAULT_COLOR_SCHEME);
			}
			catch (Exception e)
			{
				Console.Error.WriteLine(e);
			}

			CreateNewAnt();
			viewPosition = ant.Position;
			SetupView();
			ResetRender();
			StartRenderLoop();

			Task.Run(
				() =>
				{
					Thread.Sleep(250);
					this.Invoke(Refresh);
				});
		}

		private void RefreshControl(object sender, EventArgs args)
		{
			((Control)sender).Refresh();
		}

		private VectorD GetScale()
		{
			return new VectorD(
				Width / view.width,
				Height / view.height
			);
		}

		protected override void CreateHandle()
		{
			base.CreateHandle();
			Antialias = false;
		}

		private void LooseSpeedInputFocus(object sender, MouseEventArgs e)
		{
			runButton.Select();
		}

		private void SetupView()
		{
			double scale = 1 / zoomLevel;
			view = new RectD(viewPosition.x - scale * Width / 2, viewPosition.y - scale * Height / 2, Width * scale, Height * scale);
		}

		private void Zoom(object sender, MouseEventArgs e)
		{
			zoomLevel *= 1 + e.Delta * ZOOM_SPEED;
			if (zoomLevel > 2048)
				zoomLevel = 2048;
			SetupView();
			Invalidate();

			ResetRender();
		}

		private void Pan()
		{
			var delta = new VectorD(MousePosition.X - previousMousePosition.x, previousMousePosition.y - MousePosition.Y);
			double scale = 1 / zoomLevel;
			viewPosition -= scale * delta;
			previousMousePosition = new VectorD(MousePosition.X, MousePosition.Y);

			ResetRender();
		}

		// TODO: Render quickly on bounds expand
		protected override void OnRender(D2DGraphics g)
		{
			base.OnRender(g);
			if (passesToRenderLeft == 0)
				return;

			VectorD scale = GetScale();

			Bounds boardBounds = ant.board.Bounds;

			VectorD normalBoardMin = GetNormalOriginPosition(new Vector(boardBounds.xMin, boardBounds.yMin), scale);
			VectorD normalBoardMax = GetNormalOriginPosition(new Vector(boardBounds.xMax + 1, boardBounds.yMax + 1), scale);

			Rectangle screenBoardRect = new Rectangle(
				(int)normalBoardMin.x, (int)normalBoardMax.y,
				(int)(normalBoardMax.x - normalBoardMin.x), (int)(normalBoardMin.y - normalBoardMax.y));

			VectorD alignedScreenPosition = GetNormalOriginPosition(GetBoardPosition(new VectorD(), scale), scale);
			Rectangle screenRect = new Rectangle((int)alignedScreenPosition.x, (int)alignedScreenPosition.y,
				(int)(Width - alignedScreenPosition.x), (int)(Height - alignedScreenPosition.y));

			var viewBounds = new Bounds(
				(int)(view.x - 1), (int)(view.x + view.width), (int)(view.y), (int)(view.y + view.height + 1));

			ulong screenArea = (ulong)screenRect.Width * (ulong)screenRect.Height;
			ulong screenBoardArea = (ulong)screenBoardRect.Width * (ulong)screenBoardRect.Height;

			// Zoomed far out or small window
			if (screenArea == 0 || screenBoardArea == 0)
				return;

			Stopwatch w = Stopwatch.StartNew();
			{
				if (boardBounds.xMin > viewBounds.xMin || boardBounds.yMin > viewBounds.yMin
					|| boardBounds.xMax < viewBounds.xMax || boardBounds.yMax < viewBounds.yMax)
					RenderOutOfBounds(g, boardBounds, scale);

				RenderUnimportant(g,
					screenArea < screenBoardArea ? screenRect : screenBoardRect,
					scale, w);

				if (ant.Running || passesToRenderLeft >= 2)
					RenderImportant(g, boardBounds, scale, w);
			}
			w.Stop();

			VectorD antPosition = GetNormalOriginPosition(ant.Position, scale);
			var antEllipse = new D2DEllipse(
				(float)(antPosition.x + scale.x / 2),
				(float)(antPosition.y + scale.y / 2),
				(float)(scale.x / 3),
				(float)(scale.y / 3));
			g.FillEllipse(antEllipse, antFillColor);
			g.DrawEllipse(antEllipse, antBorderColor);

			if (drawBounds)
			{
				Bounds antBounds = boardBounds;
				VectorD boundMin = GetNormalOriginPosition(new Vector(antBounds.xMin, antBounds.yMax), scale);
				var pointBoundMin = new Point((int)boundMin.x, (int)boundMin.y);
				var boundSize = new Size(
					(int)((antBounds.xMax - antBounds.xMin + 1) * scale.x), (int)((antBounds.yMax - antBounds.yMin + 1) * scale.y));
				var boundsRect = new Rectangle(pointBoundMin, boundSize);
				g.DrawRectangle(boundsRect, antFillColor);
				g.DrawRectangle(boundsRect, antBorderColor, dashStyle: D2DDashStyle.Dash);
			}

			toRender?.Invoke(g);
			toRender = null;
		}

		private void RenderOutOfBounds(D2DGraphics graphics, Bounds boardBounds, VectorD scale)
		{
			VectorD a = GetNormalOriginPosition(new Vector(boardBounds.xMin, boardBounds.yMax), scale);
			VectorD c = GetNormalOriginPosition(new Vector(boardBounds.xMax, boardBounds.yMin), scale);

			graphics.FillRectangle(0, 0, (float)a.x, (float)c.y, colors[0]);
			graphics.FillRectangle((float)a.x, 0, (float)(Width - a.x), (float)a.y, colors[0]);
			graphics.FillRectangle((float)c.x, (float)a.y, (float)(Width - c.x), (float)(Height - a.y), colors[0]);
			graphics.FillRectangle(0, (float)c.y, (float)c.x, (float)(Height - c.y), colors[0]);
		}

		private IEnumerable<int> GenerateIntegers()
		{
			for (int i = 0; i < int.MaxValue; i++)
				yield return i;
		}

		private void RenderImportant(D2DGraphics graphics, Bounds boardBounds, VectorD scale, Stopwatch watch)
		{
			int stepDelta = (int)Math.Max(1, 1.0 / scale.x);

			foreach ((Vector centeredPoint, int step) in SpiralPattern().Zip(GenerateIntegers(), (v, i) => (v, i)))
			{
				if (step % stepDelta != 0)
					goto end;

				Vector position = centeredPoint + ant.Position;
				if (position.x < boardBounds.xMin || position.x > boardBounds.xMax
					|| position.y < boardBounds.yMin || position.y > boardBounds.yMax)
					break;

				VectorD normalPosition = GetNormalOriginPosition(position, scale);
				graphics.FillRectangle(
					(float)normalPosition.x, (float)normalPosition.y, (float)scale.x, (float)scale.y, colors[ant.board[position]]);

				end:
				if (watch.ElapsedMilliseconds > COMBINED_FRAME_TIME)
					break;
			}
		}

		private void RenderUnimportant(D2DGraphics graphics, Rectangle view, VectorD scale, Stopwatch watch)
		{
			int width = GetNearestBiggerPowerOf2(view.Width);
			int height = GetNearestBiggerPowerOf2(view.Height);
			int longerSize = Math.Max(width, height);

			int yMin = view.Y;
			int xMin = view.X;

			int n = longerSize * longerSize;

			while (watch.Elapsed.Milliseconds < COMBINED_FRAME_TIME - IMPORTANT_FRAME_TIME)
			{
				if (unimportantRenderPosition >= n)
				{
					unimportantRenderPosition = 0;
					if (!ant.Running && !DoneRendering)
						passesToRenderLeft--;
				}

				(int index, int blockSize, int powerOf4) = GridIndexByRenderStep(longerSize, unimportantRenderPosition);
				int x = xMin + index % longerSize;
				int y = yMin + index / longerSize;

				if (y <= Height)
				{
					if (x <= Width) {
						blockSize = Math.Min(maxUnimportantRenderBlockSize, blockSize);
						maxUnimportantRenderBlockSize = blockSize;

						Vector boardPosition = GetBoardPosition(new VectorD(x, y), scale);
						Cell cell = ant.board[boardPosition];
						D2DColor color = colors[cell];
			
						graphics.FillRectangle(x, y, blockSize, blockSize, color);

					}
					unimportantRenderPosition++;
				}
				else
				{
					unimportantRenderPosition = (int)Math.Pow(4, powerOf4 + 1);
				}
			}
		}

		/// <summary>
		/// Courtesy of Ante Buterin
		/// </summary>
		private (int index, int blockSize, int powerOf4) GridIndexByRenderStep(int viewSize, int step)
		{
			int n = viewSize * viewSize;

			int q = (int)Math.Log(step, 4);
			int p = q + 1;

			int s = (int)(step - Math.Pow(4, q));

			int dom = 1 << p;
			int sub = 1 << q;

			int segment = dom + sub;
			int remainder = s % segment;

			bool isDom = s % segment >= sub;

			int y = 2 * (s / segment) + (isDom ? 1 : 0);
			int x = isDom ? remainder - sub : 2 * remainder + 1;

			int blockSize = viewSize / dom;
			return (n / dom * y + blockSize * x, blockSize, q);
		}

		private VectorD GetNormalPosition(Vector position, VectorD scale)
		{
			var scaledPosition = new VectorD(position.x * scale.x, position.y * scale.y);
			VectorD movedPosition = scaledPosition - new VectorD(viewPosition.x * scale.x, viewPosition.y * scale.y);
			VectorD normalPosition = movedPosition + new VectorD(Width / 2.0, Height / 2.0);
			return new VectorD(normalPosition.x, Height - normalPosition.y);
		}

		private VectorD GetNormalOriginPosition(Vector position, VectorD scale)
		{
			return GetNormalPosition(position, scale) - scale / 2;
		}

		private Vector GetBoardPosition(VectorD normalPosition, VectorD scale)
		{
			var invertedPosition = new VectorD(normalPosition.x, Height - normalPosition.y);
			VectorD movedPosition = invertedPosition - new VectorD(Width / 2.0, Height / 2.0);
			VectorD scaledPosition = movedPosition + new VectorD(viewPosition.x * scale.x, viewPosition.y * scale.y);
			return new Vector(
				(int)Math.Round(scaledPosition.x / scale.x, MidpointRounding.AwayFromZero),
				(int)Math.Round(scaledPosition.y / scale.y, MidpointRounding.AwayFromZero)
			);
		}

		private int GetNearestBiggerPowerOf2(int number)
		{
			if (number < 0)
				return 0;
			number--;
			number |= number >> 1;
			number |= number >> 2;
			number |= number >> 4;
			number |= number >> 8;
			number |= number >> 16;
			return number + 1;
		}

		private void StartRenderLoop()
		{
			Task.Run(
				() =>
				{
					while (!IsDisposed)
					{
						if (!DoneRendering)
							this.Invoke(Invalidate);
						if (movingWindow || DoneRendering)
							Thread.Sleep(256);
					}
				});
		}

		private void CreateNewAnt()
		{
			if (ruleList.Text.Length <= 0)
				return;

			var rules = new List<Rotation>();
			foreach (char c in ruleList.Text)
			{
				switch (c)
				{
					case 'L':
					case 'l':
						rules.Add(Rotation._left);
						break;
					case 'R':
					case 'r':
						rules.Add(Rotation._right);
						break;
					default:
						return;
				}
			}

			ant = new Ant(rules) { stepsPerSecond = (double)speedInput.Value };
			colors = ant.Rules
				.Select(r => new D2DColor((float)random.NextDouble(), (float)random.NextDouble(), (float)random.NextDouble()))
				.ToArray();

			colors = colorScheme.colors
				.Select(c => new D2DColor(c.R / 255f, c.G / 255f, c.B / 255f))
				.Concat(GetRandomColors())
				.Take(ant.Rules.Count)
				.ToArray();

			Invalidate();

			// ReSharper disable once IteratorNeverReturns
			IEnumerable<D2DColor> GetRandomColors()
			{
				while (true)
					yield return new D2DColor((float)random.NextDouble(), (float)random.NextDouble(), (float)random.NextDouble());
			}
		}

		private void IncrementCell(MouseEventArgs mouseEventArgs)
		{
			VectorD scale = GetScale();
			var mPos = new VectorD(
				mouseEventArgs.Location.X,
				mouseEventArgs.Location.Y
			);
			Vector boardPosition = GetBoardPosition(mPos, scale);
			Cell cell = ant.board[boardPosition];
			ant.board[boardPosition] = (uint)((cell + 1) % ant.Rules.Count);

			passesToRenderLeft = 2;
			toRender += g =>
			{
				VectorD rectPos = GetNormalOriginPosition(boardPosition, scale);
				g.FillRectangle((float)rectPos.x, (float)rectPos.y, (float)scale.x, (float)scale.y, colors[ant.board[boardPosition]]);
			};
		}

		protected override bool ProcessCmdKey(ref Message msg, Keys keyData)
		{
			switch (keyData)
			{
				case Keys.W:
					viewPosition += new VectorD(0, 1);
					break;
				case Keys.A:
					viewPosition += new VectorD(-1, 0);
					break;
				case Keys.S:
					viewPosition += new VectorD(0, -1);
					break;
				case Keys.D:
					viewPosition += new VectorD(1, 0);
					break;
			}
			SetupView();
			Invalidate();

			return base.ProcessCmdKey(ref msg, keyData);
		}

		public static IEnumerable<Vector> SpiralPattern(int steps = int.MaxValue)
		{
			int x = 0, y = 0, dx = 0, dy = -1;
			for (int i = 0; i < steps; i++)
			{
				yield return new Vector(x, y);
				if (x == y || x < 0 && x == -y || x > 0 && x == 1 - y)
				{
					int temp = dx;
					dx = -dy;
					dy = temp;
				}
				x += dx;
				y += dy;
			}
		}

		private void ValidateRuleInput()
		{
			foreach (char c in ruleList.Text)
			{
				if (new[]{'l', 'L', 'r', 'R' }.All(a => a != c))
				{
					newAntButton.Enabled = false;
					ruleErrorLabel.Visible = true;
					ruleErrorLabel.Refresh();
					return;
				}
			}
			newAntButton.Enabled = true;
			ruleErrorLabel.Visible = false;
			ruleErrorLabel.Refresh();
		}

		private void ResetRender()
		{
			unimportantRenderPosition = 0;
			maxUnimportantRenderBlockSize = int.MaxValue;
			passesToRenderLeft = 1;
		}

		private void stepButton_Click(object sender, EventArgs e)
		{
			ant.Step();
			Invalidate();
			passesToRenderLeft = 2;
		}

		private void runButton_Click(object sender, EventArgs e)
		{
			ant.Run();
			passesToRenderLeft = 1;
		}

		private void stopButton_Click(object sender, EventArgs e)
		{
			ant.Stop();
			passesToRenderLeft = 2;
		}

		private void newAntButton_Click(object sender, EventArgs e)
		{
			CreateNewAnt();
			ResetRender();
		}

		private void MainForm_MouseMove(object sender, MouseEventArgs e)
		{
			if (panning)
			{
				Pan();
				SetupView();
				Invalidate();
			}
		}

		private void MainForm_MouseDown(object sender, MouseEventArgs e)
		{
			if (e.Button == MouseButtons.Left)
			{
				previousMousePosition = new VectorD(MousePosition.X, MousePosition.Y);
				panning = true;
			}
		}

		private void MainForm_MouseUp(object sender, MouseEventArgs e)
		{
			if (e.Button == MouseButtons.Left)
				panning = false;
		}

		private void speedInput_ValueChanged(object sender, EventArgs e)
		{
			ant.stepsPerSecond = (double)speedInput.Value;
		}

		private void drawBoundsCheckBox_CheckedChanged(object sender, EventArgs e)
		{
			drawBounds = drawBoundsCheckBox.Checked;
			Invalidate();
		}

		private void MainForm_Resize(object sender, EventArgs e)
		{
			SetupView();
			foreach (Control control in Controls)
				RefreshControl(control, e);
			ResetRender();
		}

		private void ruleList_KeyPress(object sender, KeyPressEventArgs e)
		{
			e.KeyChar = char.ToUpper(e.KeyChar);

			if (e.KeyChar != 'R' && e.KeyChar != 'L' && char.IsLetterOrDigit(e.KeyChar))
			{
				e.Handled = true; // skip
				return;
			}

			newAntButton.Enabled = false;
			Task.Run(
				() =>
				{
					Thread.Sleep(16);
					this.Invoke(ValidateRuleInput);
				});
		}

		private void MainForm_ResizeBegin(object sender, EventArgs e)
		{
			movingWindow = true;
		}

		private void MainForm_ResizeEnd(object sender, EventArgs e)
		{
			movingWindow = false;
			ResetRender();
		}

		private void MainForm_MouseClick(object sender, MouseEventArgs e)
		{
			if (e.Button == MouseButtons.Right)
				IncrementCell(e);
		}

		private void centerViewButton_Click(object sender, EventArgs e)
		{
			viewPosition = ant.Position;
			ResetRender();
			Invalidate();
		}

		private void editColorSchemeButton_Click(object sender, EventArgs e)
		{
			if (colorSchemeEditor == null)
			{
				colorSchemeEditor = new ColorSchemeEditor(colorScheme, random);
				colorSchemeEditor.Closing += (_, __) =>
				{
					colorSchemeEditor = null;
					Enabled = true;
					passesToRenderLeft = 2;
				};
				ant.Stop();
				Enabled = false;
			}

			colorSchemeEditor.Show();
			colorSchemeEditor.BringToFront();
		}
	}
}
