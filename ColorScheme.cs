﻿using System.Drawing;
using System.Linq;

namespace LangtonsAnt
{
	public class ColorScheme
	{
		public Color[] colors = { Color.SteelBlue };

		public int[] EncodedColors
		{
			get => colors.Select(c => c.ToArgb()).ToArray();
			set => colors = value.Select(Color.FromArgb).ToArray();
		}
	}
}
